<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="register_style.css" />
</head>

<body>
    <center>
        <!-- Use array to store info about gender and falculty -->
        <?php
        $gender = array(0 => "Nam", 1 => "Nữ");
        $falculty = array("default" => "--Chọn phân khoa--", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu")
            ?>

        <form action="" method="">
            <table>
                <tr>
                    <th></th>
                    <th colspan="2"></th>
                </tr>
                <tr>
                    <td colspan="1">
                        <label for="usn">
                            <div>Họ và tên</div>
                        </label>
                    </td>


                    <td>
                        <input type="text" name="user_name" id="usn">
                    </td>
                </tr>

                <tr>
                    <td>
                        <div>Giới tính</div>
                    </td>


                    <td style="padding-left: 20px">
                        <?php
                        for ($i = 0; $i < count($gender); $i++) {
                            ?>
                            <label class="container">
                                <?php
                                echo $gender[$i];
                                ?>
                                <input type="radio" checked="checked" name="radio">
                                <span class="checkmark"></span>
                            </label>
                            <p style="display:inline">&emsp;</p>
                            <?php
                        }
                        ?>

                        <!-- <label class="container">Nam
                            <input type="radio" checked="checked" name="radio">
                            <span class="checkmark"></span>
                        </label>
                        <p style="display:inline">&emsp;</p>
                        <label class="container">Nữ
                            <input type="radio" checked="checked" name="radio">
                            <span class="checkmark"></span>
                        </label> -->

                    </td>


                </tr>

                <tr>
                    <td>
                        <label for="khoa">
                            <div>Phân khoa</div>
                        </label>
                    </td>

                    <td>
                        <label for="khoa"></label>
                        <select name="khoa" id="khoa">
                            <?php
                            foreach ($falculty as $key => $value) {
                                ?>
                                <option value=<?php $key ?>><?php echo $value ?></option>
                                <?php
                            }
                            ?>
                            <!-- <option value="default">--Chọn phân khoa--</option>
                            <option value="MAT">Khoa học máy tính</option>
                            <option value="KDL">Khoa học vật liệu</option> -->
                        </select>
                    </td>

                </tr>

                <tr style="height:150px">
                    <th colspan="3">
                        <input type="submit" name="submit" class="button" value="Đăng ký">
                    </th>
                </tr>
            </table>
        </form>
    </center>
</body>

</html>