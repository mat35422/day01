<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="form_style.css" />
</head>

<body>
    <center>
        <p class="title_field">
            <?php
            // Set the new timezone
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $date = date('d/m/Y');
            $time = date('G:i');
            $weekday = date('w');
            $weekday_str;
            if ($weekday == 0) {
                $weekday_str = "Chủ Nhật";
            }
            else {
                $weekday_str = "thứ " . ($weekday + 1);
            }
            
            echo "Bây giờ là $time, $weekday_str ngày $date"
                ?>
        </p>

        <form action="" method="">
            <table>
                <tr>
                    <td>
                        <div>Tên đăng nhập</div>
                    </td>
                    <td>
                        <input type="text" name="user_name">
                    </td>
                </tr>

                <tr>
                    <td>
                        <div>Mật khẩu</div>
                    </td>
                    <td>
                        <input type="password" name="user_pass">

                    </td>
                </tr>

                <tr style="height:150px">
                    <th colspan="2">
                        <input type="submit" name="submit" class="button" value="Đăng nhập">
                    </th>
                </tr>
            </table>
        </form>
    </center>
</body>

</html>